/*
 * Library: crc16
 * File:    crc16_config.h
 * Author:  Igor Petrov
 *
 * This file is licensed under the MIT License as stated below
 *
 * Copyright (c) 2021 Igor Petrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Description
 * -----------
 * Universal CRC16 library that support any variation of the algorithm.
 *
 */

#ifndef CRC16_CONFIG_H_
#define CRC16_CONFIG_H_


/* SW implementation with lookup table. Fast, but large code size .
  Support 8 bit data. */
#define CRC16_FAST_TBL  (1)

/* SW implementation w/o lookup table. Slow but small code size.
  Support 8 bit data*/
#define CRC16_SLOW      (0)

/* Print table (for hard-coding in embedded applications) */
#define CRC16_PRINT_TABLE (0)

#define CRC16_MODE (CRC16_FAST_TBL)
//#define CRC16_MODE (CRC16_SLOW)

#endif /* CRC16_CONFIG_H_ */
