/*
 * Library: crc16
 * File:    crc16.c
 * Author:  Igor Petrov
 *
 * This file is licensed under the MIT License as stated below
 *
 * Copyright (c) 2021 Igor Petrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Description
 * -----------
 * Universal CRC16 library that support any variation of the algorithm.
 *
 */

#include "crc16.h"
#include "crc16_config.h"

//////////////////////////////////////////////////////////////////////////////
// Common
//////////////////////////////////////////////////////////////////////////////

// Bitwise reflect functions, could be redefined in config by HW accelerated.

#ifndef CRC16_REFLECT8
static uint8_t
reflect_8(uint8_t in)
{
  uint8_t out = 0;
  uint8_t mask = 0x80;
  uint8_t bit = 0x01;
  while (mask != 0)
    {
      if (mask & in) out |= bit;
      mask >>=1, bit <<= 1;
    }
  return out;
}
#define CRC16_REFLECT8(x) (reflect_8(x))
#endif

#ifndef CRC16_REFLECT16
static uint16_t
reflect_16(uint16_t in)
{
  uint16_t out = 0;
  uint16_t mask = 0x8000;
  uint16_t bit = 0x0001;
  while (mask != 0)
    {
      if (mask & in) out |= bit;
      mask >>=1, bit <<= 1;
    }
  return out;
}
#define CRC16_REFLECT16(x) (reflect_16(x))
#endif




static CRC16_Param_t param_cache;
static uint16_t crc16 =0;


#if (CRC16_MODE == CRC16_FAST_TBL)
/////////////////////////////////////////////////
// Table implementation
/////////////////////////////////////////////////

static uint16_t crc16_table[256] = {0x0000};

#if (CRC16_PRINT_TABLE)
#include <stdio.h>
#endif

void
crc16_init(CRC16_Param_t *param)
{
  static uint32_t prev_poly = 0xFFFFFFFF;
  param_cache = *param;
  crc16 = param_cache.init;

  // check if table is up to date
  if (prev_poly == param_cache.poly) return;
  prev_poly = param_cache.poly;

  uint16_t poly = param_cache.poly;

  for (uint16_t i = 0; i < 256; i++)
    {
      uint8_t byte = i;
      uint16_t crc = 0;

      for (uint8_t j = 0; j < 8; j++) {

        if ( (crc >> 8 ^ byte) & 0x80 )
          {
            crc = ( crc << 1 ) ^ poly;
          }
        else
          {
            crc =  (crc << 1 );
          }
        byte <<=  1;
      }
      crc16_table[i] = crc;
#if (CRC16_PRINT_TABLE)
      if (i % 8 ==0) printf("\n");
      printf("0x%04X, ",  crc16_table[i]);
#endif
    }
#if (CRC16_PRINT_TABLE)
    printf("\n\n");
#endif
}


void
crc16_update(uint8_t* data, uint32_t size)
{
    uint8_t index;
    const uint8_t reflected = param_cache.ref_in;

    while (size-- != 0) {
        uint8_t byte = *data++;
        if (reflected) byte = CRC16_REFLECT8(byte);

        index = (crc16 >> 8) ^ byte;
        crc16 = (crc16 << 8) ^ crc16_table[index];
    }
}


uint16_t
crc16_result(void)
{
  uint16_t result = crc16;
  if (param_cache.ref_out) {
      result = CRC16_REFLECT16(result);
  }

  return result ^ param_cache.xor;
}

#elif (CRC16_MODE == CRC16_SLOW)

/////////////////////////////////////////////////
// Slow implementation
/////////////////////////////////////////////////


void
crc16_init(CRC16_Param_t *param)
{
  param_cache = *param;
  crc16 = param_cache.init;
}


void
crc16_update(uint8_t* data, uint32_t size)
{
  const uint16_t poly = param_cache.poly;
  const uint8_t reflected = param_cache.ref_in;


  while (size-- != 0)
    {
      uint8_t byte = *data++;
      if (reflected) byte = CRC16_REFLECT8(byte);

      for (uint8_t j = 0; j < 8; j++)
        {
          if (((crc16 >> 8) ^ byte) & 0x80)
            {
              crc16 = (crc16 << 1) ^ poly;
            }
          else
            {
              crc16 = (crc16 << 1);
            }
          byte <<= 1;
        }
    }
}

uint16_t
crc16_result(void)
{
  uint16_t result = crc16;
  if (param_cache.ref_out) {
      result = CRC16_REFLECT16(result);
  }

  return result ^ param_cache.xor;
}





#else
 #error "Incorrect CRC16 algorithm selection"
#endif




