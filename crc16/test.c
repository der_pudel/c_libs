/*
 * Library: crc16
 * File:    test.c
 * Author:  Igor Petrov
 *
 * This file is licensed under the MIT License as stated below
 *
 * Copyright (c) 2021 Igor Petrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Description
 * -----------
 * Universal CRC16 library that support any variation of the algorithm.
 *
 */


#include <stdio.h>
#include <string.h>


#include "crc16.h"

struct test_entry {
  CRC16_Param_t param;
  uint16_t result;
};

struct test_entry test[] = {
    {{.init= 0xFFFF, .poly = 0x1021, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x29B1 }, // CRC-16/CCITT-FALSE
    {{.init= 0x0000, .poly = 0x8005, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0xBB3D }, // CRC-16/ARC
    {{.init= 0x1D0F, .poly = 0x1021, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0xE5CC }, // CRC-16/AUG-CCITT
    {{.init= 0x0000, .poly = 0x8005, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0xFEE8 }, // CRC-16/BUYPASS
    {{.init= 0xFFFF, .poly = 0xC867, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x4C06 }, // CRC-16/CDMA2000
    {{.init= 0x800D, .poly = 0x8005, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x9ECF }, // CRC-16/DDS-110
    {{.init= 0x0000, .poly = 0x0589, .xor = 0x0001, .ref_in = 0, .ref_out = 0}, 0x007E }, // CRC-16/DECT-R
    {{.init= 0x0000, .poly = 0x0589, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x007F }, // CRC-16/DECT-X
    {{.init= 0x0000, .poly = 0x3D65, .xor = 0xFFFF, .ref_in = 1, .ref_out = 1}, 0xEA82 }, // CRC-16/DNP
    {{.init= 0x0000, .poly = 0x3D65, .xor = 0xFFFF, .ref_in = 0, .ref_out = 0}, 0xC2B7 }, // CRC-16/EN-13757
    {{.init= 0xFFFF, .poly = 0x1021, .xor = 0xFFFF, .ref_in = 0, .ref_out = 0}, 0xD64E }, // CRC-16/GENIBUS
    {{.init= 0x0000, .poly = 0x8005, .xor = 0xFFFF, .ref_in = 1, .ref_out = 1}, 0x44C2 }, // CRC-16/MAXIM
    {{.init= 0xFFFF, .poly = 0x1021, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0x6F91 }, // CRC-16/MCRF4XX
    {{.init= 0xB2AA, .poly = 0x1021, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0x63D0 }, // CRC-16/RIELLO
    {{.init= 0x0000, .poly = 0x8BB7, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0xD0DB }, // CRC-16/T10-DIF
    {{.init= 0x0000, .poly = 0xA097, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x0FB3 }, // CRC-16/TELEDISK
    {{.init= 0x89EC, .poly = 0x1021, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0x26B1 }, // CRC-16/TMS37157
    {{.init= 0xFFFF, .poly = 0x8005, .xor = 0xFFFF, .ref_in = 1, .ref_out = 1}, 0xB4C8 }, // CRC-16/USB
    {{.init= 0xC6C6, .poly = 0x1021, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0xBF05 }, // CRC-A
    {{.init= 0x0000, .poly = 0x1021, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0x2189 }, // CRC-16/KERMIT
    {{.init= 0xFFFF, .poly = 0x8005, .xor = 0x0000, .ref_in = 1, .ref_out = 1}, 0x4B37 }, // CRC-16/MODBUS
    {{.init= 0xFFFF, .poly = 0x1021, .xor = 0xFFFF, .ref_in = 1, .ref_out = 1}, 0x906E }, // CRC-16/X-25
    {{.init= 0x0000, .poly = 0x1021, .xor = 0x0000, .ref_in = 0, .ref_out = 0}, 0x31C3 }, // CRC-16/XMODEM
};




#define ARR_SIZE(x) (sizeof(x)/sizeof(x[0]))

int main()
{
  char *test_str = "123456789";
  uint32_t len = strlen(test_str);
  uint32_t part = len /2;  /* test continuation of calculation */

  uint8_t result = 1;

  for(uint32_t i = 0; i < ARR_SIZE(test); i++)
    {
      crc16_init(&test[i].param);
      crc16_update((uint8_t *)test_str, part);
      crc16_update((uint8_t *)test_str + part,len-part);
      uint16_t crc16 = crc16_result();
      if (crc16 != test[i].result)
        {
          printf("Fail at %d, expected %04X, got %04X\n", i, test[i].result, crc16);
          result = 0;
        }

    }
  printf("Test %s\n", result ? "passed" : "failed");


  return 0;
}
