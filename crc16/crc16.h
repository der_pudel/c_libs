/*
 * Library: crc16
 * File:    crc16.h
 * Author:  Igor Petrov
 *
 * This file is licensed under the MIT License as stated below
 *
 * Copyright (c) 2021 Igor Petrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Description
 * -----------
 * Universal CRC16 library that support any variation of the algorithm.
 *
 */

#ifndef CRC16_H_
#define CRC16_H_

#include <stdint.h>


typedef struct {
  uint16_t init;         /* Initial value */
  uint16_t xor;          /* Output XOR */
  uint16_t poly;         /* generator polynomial */
  uint16_t ref_in  : 1;  /* Input byte reflected */
  uint16_t ref_out : 1;  /* Result reflected */
} CRC16_Param_t;

void
crc16_init(CRC16_Param_t *param);

void
crc16_update(uint8_t* data, uint32_t size);

uint16_t
crc16_result(void);


#endif /* CRC16_H_ */
